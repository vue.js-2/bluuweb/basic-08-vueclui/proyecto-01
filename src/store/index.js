import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    count: 0,
    razonSuma: 5,
    razonResta: 4,
  },
  getters: {},
  mutations: {
    aumentar(state, n) {
      state.count += n;
    },
    disminuir(state, n) {
      state.count -= n;
    },
  },
  actions: {},
  modules: {},
});
